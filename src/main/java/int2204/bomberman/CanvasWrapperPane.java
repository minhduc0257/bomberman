package int2204.bomberman;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;

public class CanvasWrapperPane extends Pane {
    private final Canvas canvas;
    public CanvasWrapperPane(double width, double height) {
        canvas = new Canvas(width,  height);
        this.getChildren().add(canvas);
    }
    
    public Canvas getCanvas() {
        return canvas;
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        final double w = snapSizeX(getWidth()) - x - snappedRightInset();
        final double h = snapSizeY(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
    }
}
