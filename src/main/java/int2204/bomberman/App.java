package int2204.bomberman;

import int2204.bomberman.BaseGame.Game;
import int2204.bomberman.BaseGame.LevelLayout;
import int2204.bomberman.BaseGame.LevelLayoutContainer;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class App extends Application {
    public static Game game = new Game();
    public static LevelLayout currentLevel;

    //Called every frame
    public static void DrawStateOntoCanvas(Canvas canvas) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        double height = context.getCanvas().getHeight();
        double width = context.getCanvas().getWidth();

        context.setFill(Color.web("rgb(194, 223, 242)"));
        context.fillRect(0, 0, width, height);

        game.drawToCanvas(context);
    }

    @Override
    public void start(Stage stage) throws Exception {
        var screen = Screen.getPrimary().getBounds();
        
        var root = new CanvasWrapperPane(screen.getMaxX(), screen.getMaxY());
        var scene = new Scene(root, root.getMaxWidth(), root.getMaxHeight());

        var controlPane = new StackPane();
        var initialScene = new Scene(controlPane, screen.getMaxX(), screen.getMaxY());
        var playButton = new Button("Play");
        StackPane.setAlignment(playButton, Pos.CENTER);
        playButton.setOnAction(actionEvent -> stage.setScene(scene));
        controlPane.getChildren().add(playButton);
        stage.setScene(initialScene);
        
        stage.setTitle("Bomberman");
//        stage.setScene(scene);

        scene.addEventHandler(KeyEvent.KEY_RELEASED,keyEvent -> game.handleKeyRelease(keyEvent));

        scene.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> game.handleKeyPress(keyEvent));

        var animationTimer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                game.updateAction();
                DrawStateOntoCanvas(root.getCanvas());
            }
        };

        animationTimer.start();

        stage.show();
    }

    public static void main(String[] args) {
        LevelLayoutContainer level = new LevelLayoutContainer();
        game.goToLevel(0);
        launch();
    }
}
