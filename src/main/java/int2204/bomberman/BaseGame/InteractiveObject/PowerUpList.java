package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.Random;

public class PowerUpList {
    private ArrayList<PowerUp> list;
    //In %
    private final int DROP_CHANCE = 20;

    public void addPowerUp(Point tilePos, int type) {
        list.add(new PowerUp(tilePos, type));
    }

    public void checkPlayerOnTop(Player player) {
        ArrayList<PowerUp> consumed= new ArrayList<>();
        for(PowerUp powerUp : list) {
            if(player.getTile().isEqual(powerUp.getTile())) {
                powerUp.givePower(player);
                consumed.add(powerUp);
            }
        }
        for(PowerUp powerUp : consumed) {
            list.remove(powerUp);
        }
    }

    public void addRandom(Point tilePos) {
        Random random = new Random();
        int roll = random.nextInt(100);
        if(roll < DROP_CHANCE) {
            roll = random.nextInt(PowerUp.POWER_COUNT);
            addPowerUp(tilePos, roll);
        }
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        for(PowerUp powerUp : list) {
            powerUp.drawToCanvas(context, sprite);
        }
    }

    public void checkDestroyed(ExplosionList explosionList) {
        ArrayList<PowerUp> destroyed = new ArrayList<>();
        for(PowerUp powerUp : list) {
            if(explosionList.checkEntityOnTop(powerUp.getTile())) {
                destroyed.add(powerUp);
            }
        }
        for(PowerUp powerUp : destroyed) {
            list.remove(powerUp);
        }
    }

    public PowerUpList() {
        this.list = new ArrayList<>();
    }
}
