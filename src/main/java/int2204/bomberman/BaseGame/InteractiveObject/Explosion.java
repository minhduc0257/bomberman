package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Entity;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

public class Explosion extends Entity {
    //How long the explosion last
    public final int EXPLOSION_TIMER = 20;
    int timer;

    public boolean tickDown() {
        return --timer == 0;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        context.drawImage(sprite.getSprite(GameSprite.EXPLOSION).getImage(),
                getPos().getX(), getPos().getY());
    }

    public Explosion(Point tilePos) {
        super(tilePos);
        timer = EXPLOSION_TIMER;
    }
}
