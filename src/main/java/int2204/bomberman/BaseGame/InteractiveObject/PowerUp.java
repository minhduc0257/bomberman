package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Entity;
import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

public class PowerUp extends Entity {
    public static final int POWER_COUNT = 3;
    public static final int POWER_EXPLOSION_UP = 0;
    public static final int POWER_BOMB_UP = 1;
    public static final int POWER_SPEED_UP = 2;

    private int type;

    public void givePower(Player player) {
        switch (type){
            case POWER_EXPLOSION_UP -> player.explosionsUp();
            case POWER_BOMB_UP -> player.bombUp();
            case POWER_SPEED_UP -> player.speedUp();
        }
    }

    public PowerUp(Point tilePos, int type) {
        super(tilePos);
        this.type = type;
    }

    public void draw(final int TYPE, GraphicsContext context, GameSprite sprite) {
        context.drawImage(sprite.getSprite(TYPE).getImage(),
                getPos().getX(), getPos().getY());
    }
    
    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        switch (type) {
            case POWER_EXPLOSION_UP -> draw(GameSprite.POWER_EXPLOSION, context, sprite);
            case POWER_BOMB_UP -> draw(GameSprite.POWER_BOMB, context, sprite);
            case POWER_SPEED_UP -> draw(GameSprite.POWER_SPEED, context, sprite);
        }
    }

    public void destroy() {}

}
