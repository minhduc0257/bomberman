package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;

public class ExplosionList {
    ArrayList<Explosion> list;

    public void tickDown() {
        list.removeIf(Explosion::tickDown);
    }

    public void addExplosion(Point pos) {
        list.add(new Explosion(pos));
    }

    public boolean checkEntityOnTop(Point tilePos) {
        for (Explosion e : list) {
            if (tilePos.isEqual(e.getTile())) {
                return true;
            }
        }
        return false;
    }

    public ExplosionList() {
        this.list = new ArrayList<>();
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        for (Explosion e : list) {
            e.drawToCanvas(context, sprite);
        }
    }
}
