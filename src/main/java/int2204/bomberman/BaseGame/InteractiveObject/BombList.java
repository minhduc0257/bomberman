package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.Game;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.LevelLayout;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Stores all the bombs created in the level.
 */
public class BombList {
    ArrayList<Bomb> list;
    //If true, will check if player is on top of a bomb
    boolean hasOnTop;

    public void addBomb(Point tile, int size, int timer) {
        if (getBombAtTile(tile) == null) {
            System.out.println("Placed bomb of size " + size);
            list.add(new Bomb(tile, size, timer));
            hasOnTop = true;
        }
    }

    //
    public Bomb getBombAtPixel(Point pos) {
        Point tilePos = new Point(pos.getX() / GameSprite.SIZE, pos.getY() / GameSprite.SIZE);
        for (Bomb b : list) {
            if (b.getTile().isEqual(tilePos)) {
                return b;
            }
        }
        return null;
    }

    public Bomb getBombAtTile(Point tilePos) {
        for (Bomb b : list) {
            if (b.getTile().isEqual(tilePos)) {
                return b;
            }
        }
        return null;
    }

    /**
     * Check if player is still on top of a bomb or not.
     * If not than player can't walk on top of it anymore.
     */
    public void checkPlayerOnTop(final Player player) {
        for (Bomb bomb : list) {
            checkOnTopPlayer(player);
        }
    }

    /**
     * Tick down all bombs.
     */
    public void tickDown(LevelLayout level, ExplosionList explosionList, PowerUpList powerUpList, Portal portal) {
        ArrayList<Point> exploded = new ArrayList<>();
        for (Bomb bomb : list) {
            if (bomb.tickDown()) {
                exploded.add(bomb.getTile());
            }
        }
        while (!exploded.isEmpty()) {
            ArrayList<Point> newExploded = new ArrayList<>();
            for (Point b : exploded) {
                System.out.println(b);
                Bomb bomb = getBombAtTile(b);
                if (bomb != null) {
                    bomb.destroy();
                    explosionList.addExplosion(bomb.getTile());
                    Point pos = bomb.getTile();
                    boolean[] sideFinished = {false, false, false, false};
                    //Destroy blocks in the explosion's path
                    for (int i = 1; i <= bomb.getExplosionSize(); i++) {
                        for (int j = 0; j < sideFinished.length; j++) {
                            if (!sideFinished[j]) {
                                Point tilePos = new Point(
                                        pos.getX() + Game.dir[j].getX() * i,
                                        pos.getY() + Game.dir[j].getY() * i);
                                char tile = level.getTile(tilePos);
                                level.destroyTile(tilePos);
                                if (LevelLayout.isCollision(tile)) {
                                    if(LevelLayout.isDestructible(tile)) {
                                        if(!portal.getTile().isEqual(tilePos)) {
                                            powerUpList.addRandom(tilePos);
                                        }
                                    }
                                    sideFinished[j] = true;
                                } else {
                                    Bomb currentBomb = getBombAtTile(tilePos);
                                    if (currentBomb != null) {
                                        newExploded.add(tilePos);
                                        sideFinished[j] = true;
                                    }
                                    explosionList.addExplosion(tilePos);
                                }
                            }
                        }
                    }
                    list.remove(bomb);
                }
            }
            exploded.clear();
            exploded.addAll(newExploded);
        }
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        for (Bomb bomb : list) {
            bomb.drawToCanvas(context, sprite);
        }
    }

    public int getBombCount() {
        return list.size();
    }

    /**
     * Check if a bomb is on top of a player
     */
    public void checkOnTopPlayer(Player player) {
        if (player.isAlive()) {
            if (hasOnTop) {
                boolean isOnTop = false;
                for (Bomb bomb : list) {
                    if (bomb.checkPlayerOnTop(player)) {
                        isOnTop = true;
                    }
                }
                hasOnTop = isOnTop;
            }
        }
    }

    public BombList() {
        list = new ArrayList<>();
        hasOnTop = true;
    }
}
