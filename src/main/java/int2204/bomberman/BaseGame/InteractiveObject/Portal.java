package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Entity;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

/**
 * Go to the next level when reached by player after defeating all enemies.
 */
public class Portal extends Entity {
    private boolean opened;

    public Portal(Point tilePos) {
        super(tilePos);
        opened = false;
    }

    public Portal() {
        super(new Point(0, 0));
    }

    public void activate() {
        opened = true;
    }

    public boolean isOpened() {
        return opened;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        int type = opened ? GameSprite.PORTAL_OPEN : GameSprite.PORTAL_CLOSED;
        context.drawImage(sprite.getSprite(type).getImage(),
                getPos().getX(), getPos().getY());
    }
}
