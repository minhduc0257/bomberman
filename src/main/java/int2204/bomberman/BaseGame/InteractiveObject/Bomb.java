package int2204.bomberman.BaseGame.InteractiveObject;

import int2204.bomberman.BaseGame.Entity.Entity;
import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

/**
 * Bomb created by the character.
 */
public class Bomb extends Entity {
    //Size of the bomb's explosion, assigned when created.
    private final int EXPLOSION_SIZE;
    //Timer of the bomb, can be changed to adjust game difficulty.
    private int timer;
    //True if the player is standing on top of the bomb, canceling collision until player leaves
    private boolean playerOnTop;

    public Bomb(Point tilePos, int EXPLOSION_SIZE, int timer) {
        super(tilePos);
        this.EXPLOSION_SIZE = EXPLOSION_SIZE;
        this.timer = timer;
        this.playerOnTop = true;
    }

    public boolean checkPlayerOnTop(final Player p) {
        boolean verCheck = p.getPos().getX() < this.getMaxPos().getX() &&
                p.getMaxPos().getX() > this.getPos().getX();
        boolean horCheck = p.getPos().getY() < this.getMaxPos().getY() &&
                p.getMaxPos().getY() > this.getPos().getY();
        boolean isOnTop = verCheck && horCheck;
        if(!isOnTop) {
            playerOnTop = false;
        }
        return playerOnTop;
    }

    public boolean isPlayerOnTop() {
        return playerOnTop;
    }

    public boolean tickDown() {
        return --timer == 0;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        context.drawImage(sprite.getSprite(GameSprite.BOMB).getImage(),
                getPos().getX(), getPos().getY());
    }

    public int getExplosionSize() {
        return EXPLOSION_SIZE;
    }

    /**
     * Destroy the bomb, creates an explosion.
     */
    public void destroy() {
        System.out.println("Bomb with size of " + EXPLOSION_SIZE +
            " at " + getPos().getX() / GameSprite.SIZE + " " + getPos().getY() / GameSprite.SIZE + " exploded");
    }
}
