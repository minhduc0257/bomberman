package int2204.bomberman.BaseGame;

/**
 * Generic game object
 */
public class GameObject {
    public GameObject() {}

    //can be destroyed be bombs
    public static boolean DESTRUCTIBLE = false;
    public static boolean COLLISION = false;
}
