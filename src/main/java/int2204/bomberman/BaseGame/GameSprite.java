package int2204.bomberman.BaseGame;

import int2204.bomberman.App;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

/**
 * Storing general sprite data
 */
public class GameSprite {
    //Global sprite size
    public static final int SIZE = 32;

    public final Image[] sprite;

    public static final int PLAYER = 0;
    public static final int BOMB = 1;
    public static final int FLOOR = 2;
    public static final int WALL = 3;
    public static final int BLOCK = 4;
    public static final int EXPLOSION = 5;
    public static final int PORTAL_CLOSED = 6;
    public static final int PORTAL_OPEN = 7;
    public static final int POWER_EXPLOSION = 8;
    public static final int POWER_BOMB = 9;
    public static final int POWER_SPEED = 10;
    public static final int ENEMY_1 = 11;
    public static final int ENEMY_2 = 12;

    public static final String PATH_PLAYER = "sprites/player.png";
    public static final String PATH_BOMB = "sprites/bomb.png";
    public static final String PATH_FLOOR = "sprites/floor.png";
    public static final String PATH_BLOCK = "sprites/block.png";
    public static final String PATH_WALL = "sprites/wall.png";
    public static final String PATH_EXPLOSION = "sprites/explosion.png";
    public static final String PATH_PORTAL_CLOSED = "sprites/portalClosed.png";
    public static final String PATH_PORTAL_OPEN = "sprites/portalOpen.png";
    public static final String PATH_POWER_EXPLOSION = "sprites/powerupExplosion.png";
    public static final String PATH_POWER_BOMB = "sprites/powerupBomb.png";
    public static final String PATH_POWER_SPEED = "sprites/powerupSpeed.png";
    public static final String PATH_ENEMY_1 = "sprites/enemy1.png";
    public static final String PATH_ENEMY_2 = "sprites/enemy2.png";

    public GameSprite(Image[] sprite) {
        this.sprite = sprite;
    }

    public GameSprite() {
        this.sprite = new Image[13];
        setSprite(PLAYER, PATH_PLAYER);
        setSprite(BOMB, PATH_BOMB);
        setSprite(FLOOR, PATH_FLOOR);
        setSprite(WALL, PATH_WALL);
        setSprite(BLOCK, PATH_BLOCK);
        setSprite(EXPLOSION, PATH_EXPLOSION);
        setSprite(PORTAL_CLOSED, PATH_PORTAL_CLOSED);
        setSprite(PORTAL_OPEN, PATH_PORTAL_OPEN);
        setSprite(POWER_EXPLOSION, PATH_POWER_EXPLOSION);
        setSprite(POWER_BOMB, PATH_POWER_BOMB);
        setSprite(POWER_SPEED, PATH_POWER_SPEED);
        setSprite(ENEMY_1, PATH_ENEMY_1);
        setSprite(ENEMY_2, PATH_ENEMY_2);
    }

    private void setSprite(final int NAME , final String PATH) {
        sprite[NAME] = new Image(Objects.requireNonNull(
                App.class.getResourceAsStream(PATH)));
    }

    public ImageView getSprite(final int NAME) {
        return Assets.GetScaledImageView(sprite[NAME], SIZE, SIZE);
    }
}