package int2204.bomberman.BaseGame;

import int2204.bomberman.App;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.Vector;

public class LevelLayoutContainer {
    private final Vector<LevelLayout> list;
    private int numberOfLevel;

    public LevelLayoutContainer(Vector<LevelLayout> list) {
        this.list = list;
    }

    public LevelLayoutContainer() {
        this.list = new Vector<>();
        numberOfLevel = 0;
    }

    public void getLevelFromFile(String filePath) {
        InputStreamReader file = new InputStreamReader
                (Objects.requireNonNull(App.class.getResourceAsStream(filePath)));
        Scanner reader = new Scanner(file);
        //Read all levels
        while (reader.hasNextLine()) {
            ++numberOfLevel;
            //Get level info
            String[] split = reader.nextLine().split(" ");
            int levelRow = Integer.parseInt(split[0]);
            int levelCol = Integer.parseInt(split[1]);
            //Read current level layout
            ArrayList<ArrayList<Character>> currentLayout = new ArrayList<>();
            for (int i = 0; i < levelRow; i++) {
                String currentLine = reader.nextLine();
                currentLayout.add(new ArrayList<>());
                for (char c : currentLine.toCharArray()) {
                    currentLayout.get(i).add(c);
                }
            }
            list.add(new LevelLayout(currentLayout, levelRow, levelCol));
        }
    }

    public int getNumberOfLevel() {
        return numberOfLevel;
    }

    public LevelLayout getLevel(int levelNum) {
        return list.get(levelNum);
    }

    public int getSize() {
        return list.size();
    }

    /**
     * Print all levels as text on commandline.
     */
    public void printAllLevel() {
        try {
            for (LevelLayout l : list) {
                System.out.println(l);
            }
        } catch (NullPointerException e) {
            System.out.println("No level to display.");
        }
    }

    public void clear() {
        list.clear();
    }
}
