package int2204.bomberman.BaseGame;

public class Point {
    private int x;
    private int y;

    public void setPoint(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(Point p) {
        x = p.getX();
        y = p.getY();
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point sum(Point p) {
        return new Point(x + p.getX(), y + p.getY());
    }

    public boolean isEqual(Point p) {
        return (p.getX() == this.x) && (p.getY() == this.y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "{" + x + ", " + y + '}';
    }
}

