//package int2204.bomberman.BaseEntities;
//
//import int2204.bomberman.BaseGame.Assets;
//import int2204.bomberman.HelloApplication;
//import int2204.bomberman.PlayField;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.util.Pair;
//
//import java.util.Objects;
//
//public class Portal extends AbstractEntity {
//    public static final Image Sprite = new Image(Objects.requireNonNull(HelloApplication.class.getResourceAsStream("sprites/portal.png")));
//    public static ImageView GetScaledImageView(double width, double height)
//    {
//        return Assets.GetScaledImageView(Sprite, width, height);
//    }
//    public Portal(int x, int y, boolean DESTRUCTIBLE, boolean COLLISION) {
//        super(x, y, DESTRUCTIBLE, COLLISION);
//    }
//
//    @Override
//    public void OnCollision(AbstractEntity entity, PlayField playField) {
//        if (entity instanceof Player)
//        {
//            var currentPortalIndex = playField.portals.indexOf(this);
//            var toPortal = playField.portals.get((currentPortalIndex + 1) % playField.portals.size());
//
//            int[] Xaxes = new int[] {(int) toPortal.x - 1, (int) toPortal.x + 1};
//            int[] Yaxes = new int[] {(int) toPortal.y - 1, (int) toPortal.y + 1};
//            boolean done = false;
//
//            for (int a = 0 ; a < 2 ; a++)
//            {
//                if (playField.Move(new Pair<>((double) Xaxes[a], toPortal.y)))
//                {
//                    done = true;
//                    break;
//                }
//            };
//
//            if (!done)
//            for (int a = 0 ; a < 2 ; a++)
//            {
//                if (playField.Move(new Pair<>(toPortal.x, (double) Yaxes[a])))
//                {
//                    break;
//                }
//            };
//
//        }
//        super.OnCollision(entity, playField);
//    }
//
//    public void destroy() {
//
//    }
//}
