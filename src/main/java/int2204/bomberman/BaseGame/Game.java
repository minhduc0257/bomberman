package int2204.bomberman.BaseGame;


import int2204.bomberman.BaseGame.Entity.Enemy;
import int2204.bomberman.BaseGame.Entity.EnemyList;
import int2204.bomberman.BaseGame.Entity.Player;
import int2204.bomberman.BaseGame.InteractiveObject.*;
import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Handle game.
 */
public class Game {
    public static Point[] dir = {new Point(-1, 0), new Point(1, 0), new Point(0, -1), new Point(0, 1)};
    private final boolean[] movementKeyPressed = {false, false, false, false};
    private boolean spacePressed = false;
    private boolean rPressed = false;

    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int UP = 2;
    public static final int DOWN = 3;

    public static final String MAP_PATH = "map.txt";

    private final GameSprite sprite;

    private final LevelLayoutContainer levelContainer;
    private LevelLayout currentLevel;
    private int levelNum;
    private Portal portal;
    private Player player;
    private BombList bombList;
    private ExplosionList explosionList;
    private PowerUpList powerUpList;
    //private EnemyList enemyList;

    public void readFile(String fileName) {
        levelContainer.getLevelFromFile(fileName);
        System.out.println("Found " + levelContainer.getNumberOfLevel() + " level.");
        levelContainer.printAllLevel();
    }
    //----------------------------------------------

    /**
     * Read key press.
     */
    public void handleKeyPress(KeyEvent keyEvent) {
        KeyCode keyCode = keyEvent.getCode();
        switch (keyCode) {
            case UP -> pressMovementKey(Game.UP);
            case DOWN -> pressMovementKey(Game.DOWN);
            case LEFT -> pressMovementKey(Game.LEFT);
            case RIGHT -> pressMovementKey(Game.RIGHT);
            case SPACE -> spaceDown();
            case R -> rDown();
        }
    }

    /**
     * Read key release.
     */
    public void handleKeyRelease(KeyEvent keyEvent) {
        KeyCode keyCode = keyEvent.getCode();
        switch (keyCode) {
            case UP -> releaseMovementKey(Game.UP);
            case DOWN -> releaseMovementKey(Game.DOWN);
            case LEFT -> releaseMovementKey(Game.LEFT);
            case RIGHT -> releaseMovementKey(Game.RIGHT);
            case SPACE -> spacePressed = false;
            case R -> rPressed = false;
        }
    }

    private void pressMovementKey(int CODE) {
        if (!movementKeyPressed[CODE]) {
            player.addDir(dir[CODE].getX(), dir[CODE].getY());
            movementKeyPressed[CODE] = true;
        }
    }

    private void releaseMovementKey(int CODE) {
        if (movementKeyPressed[CODE]) {
            player.addDir(-dir[CODE].getX(), -dir[CODE].getY());
            movementKeyPressed[CODE] = false;
        }
    }

    private void releaseAllPlayerAction() {
        Arrays.fill(movementKeyPressed, false);
        spacePressed = false;
    }

    private void spaceDown() {
        if (!spacePressed) {
            spacePressed = true;
            if (bombList.getBombCount() < player.getCurrentBombCount()) {
                bombList.addBomb(
                        player.getTile(),
                        player.getCurrentBombSize(),
                        player.getBaseTimer());
            }
        }
    }

    private void rDown() {
        if (!rPressed) {
            rPressed = true;
            reset(false);
        }
    }
    //----------------------------------------------

    /**
     * Update entities to next frame.
     */
    public void updateAction() {
        //Player section
        if (player.isAlive()) {
            int playerSpeed = player.getSpeed();

            int dX = player.getDir().getX();
            int dY = player.getDir().getY();

            for (int i = 0; i < playerSpeed; i++) {
                boolean verOk = false;
                boolean horOk = false;
                boolean canMove;
                do {
                    canMove = false;
                    if (!horOk && checkNextMove(player.getPos(), player.getMaxPos(), dX, 0)) {
                        horOk = true;
                        canMove = true;
                        player.moveBy(dX, 0);
                    }
                    if (!verOk && checkNextMove(player.getPos(), player.getMaxPos(), 0, dY)) {
                        verOk = true;
                        canMove = true;
                        player.moveBy(0, dY);
                    }
                } while (canMove);
            }
        }
        //Enemy section
//        for (Enemy enemy : enemyList.getList()) {
//            if (enemy.finishedPath()) {
//                switch (enemy.getType()) {
//                    case Enemy.TYPE1 -> enemy.addPath(getRandomDirection(enemy.getTile()));
//                    case Enemy.TYPE2 -> {
//                    }
//                }
//            }
//            if (enemy.getCurrentMoveLeft() <= 0) {
//                enemy.goToNextTile();
//            }
//            int enemySpeed = enemy.getSpeed();
//
//            int dX = enemy.getCurrentDir().getX();
//            int dY = enemy.getCurrentDir().getY();
//            Point originalPos = enemy.getPos();
//            int frameCantMove = 3;
//            for (int i = 0; i < enemySpeed; i++) {
//                boolean verOk = false;
//                boolean horOk = false;
//                boolean canMove;
//                do {
//                    canMove = false;
//                    if (!horOk && checkNextMove(enemy.getPos(), enemy.getMaxPos(), dX, 0)) {
//                        horOk = true;
//                        canMove = true;
//                        enemy.moveBy(dX, 0);
//                    }
//                    if (!verOk && checkNextMove(enemy.getPos(), enemy.getMaxPos(), 0, dY)) {
//                        verOk = true;
//                        canMove = true;
//                        enemy.moveBy(0, dY);
//                    }
//                } while (canMove);
//                if(enemy.getPos().isEqual(originalPos)) {
//                    if(--frameCantMove == 0) {
//                        enemy.setCurrentMoveLeft(0);
//                    }
//                }
//            }
//            enemy.setCurrentMoveLeft(enemy.getCurrentMoveLeft() - enemySpeed);
//        }
//        for (Enemy enemy : enemyList.getList()) {
//            int enemySpeed = enemy.getSpeed();
//            if(enemy.getCurrentDir() == null) {
//                enemy.addPath(getRandomDirection(enemy.getTile()));
//                enemy.goToNextTile();
//            }
//            int dX = enemy.getCurrentDir().getX();
//            int dY = enemy.getCurrentDir().getY();
//            Point originalPos = enemy.getPos();
//            for (int i = 0; i < enemySpeed; i++) {
//                boolean verOk = false;
//                boolean horOk = false;
//                boolean canMove;
//                do {
//                    canMove = false;
//                    if (!horOk && checkNextMove(enemy.getPos(), enemy.getMaxPos(), dX, 0)) {
//                        horOk = true;
//                        canMove = true;
//                        enemy.moveBy(dX, 0);
//                    }
//                    if (!verOk && checkNextMove(enemy.getPos(), enemy.getMaxPos(), 0, dY)) {
//                        verOk = true;
//                        canMove = true;
//                        enemy.moveBy(0, dY);
//                    }
//                } while (canMove);
//            }
//            if(enemy.getPos().isEqual(originalPos)) {
//                enemy.addPath(getRandomDirection(enemy.getTile()));
//                enemy.goToNextTile();
//            }
//        }
        //Portal section
        if (portal.getTile().isEqual(player.getTile())) {
            player.destroy();
            reset(true);
        }
        //Bomb section
        bombList.tickDown(currentLevel, explosionList, powerUpList, portal);
        //Explosion section
        explosionList.tickDown();
        //Check all collisions
        powerUpList.checkDestroyed(explosionList);
        powerUpList.checkPlayerOnTop(player);
        bombList.checkOnTopPlayer(player);
        if (player.isAlive()) {
            if (explosionList.checkEntityOnTop(player.getTile())) {
                System.out.println("Game over");
                gameOver();
            }
        }
    }

    /**
     * Get a random direction that's not blocked form a position.
     */
    private Point getRandomDirection(Point tilePos) {
        ArrayList<Point> list = new ArrayList<>();
        for (Point point : dir) {
            if (!LevelLayout.isCollision(
                    currentLevel.getTile(
                            tilePos.getX() + point.getX(),
                            tilePos.getY() + point.getY()))) {
                list.add(point);
            }
        }
        Random random = new Random();
        int roll = random.nextInt(list.size());
        Point ret = list.get(roll);
        return list.get(roll);
    }

    private boolean checkNextMove(Point p1, Point p2, int dirX, int dirY) {
        Point[] points = new Point[4];
        points[0] = new Point(p1.getX() + dirX, p1.getY() + dirY);
        points[1] = new Point(p1.getX() + dirX, p2.getY() + dirY);
        points[2] = new Point(p2.getX() + dirX, p1.getY() + dirY);
        points[3] = new Point(p2.getX() + dirX, p2.getY() + dirY);

        for (Point currentPoint : points) {
            Bomb currentBomb = bombList.getBombAtPixel(currentPoint);
            if (currentBomb != null) {
                if (!currentBomb.isPlayerOnTop()) {
                    return false;
                }
            }
            if (LevelLayout.isCollision(currentLevel.getTileFromPixel(currentPoint))) {
                return false;
            }
        }
        return true;
    }

    //----------------------------------------------
    public void drawToCanvas(GraphicsContext context) {
        currentLevel.drawToCanvas(context, sprite);
        if (currentLevel.getTile(portal.getTile()) == LevelLayout.FLOOR) {
            portal.drawToCanvas(context, sprite);
        }
        //enemyList.drawToCanvas(context, sprite);
        bombList.drawToCanvas(context, sprite);
        explosionList.drawToCanvas(context, sprite);
        powerUpList.drawToCanvas(context, sprite);
        player.drawToCanvas(context, sprite);
    }

    //----------------------------------------------
    private void gameOver() {
        releaseAllPlayerAction();
        player.destroy();
    }

    private void reset(boolean levelCompleted) {
        if (!player.isAlive()) {
            if (!levelCompleted) {
                System.out.println("Reset game");
                levelContainer.clear();
                player = new Player();
                readFile(MAP_PATH);
                goToLevel(0);
            } else {
                ++levelNum;
                player.reset();
                if (levelNum >= levelContainer.getSize()) {
                    releaseAllPlayerAction();
                    if (player.isAlive()) System.out.println("YOU WON");
                    Platform.exit();
                } else {
                    goToLevel(levelNum);
                }
            }
        }
    }

    public void goToLevel(int levelNum) {
        System.out.println("Go to level " + levelNum);
        this.levelNum = levelNum;
        currentLevel = new LevelLayout(levelContainer.getLevel(levelNum));
        //Reset values
        releaseAllPlayerAction();
        bombList = new BombList();
        explosionList = new ExplosionList();
        powerUpList = new PowerUpList();
        //Read entities' data
        for (int row = 0; row < currentLevel.getRow(); row++) {
            for (int col = 0; col < currentLevel.getCol(); col++) {
                //Currently, only has one entity so only 1 switch case
                //Will make more in the future
                switch (currentLevel.getTile(row, col)) {
                    case (LevelLayout.PLAYER) -> {
                        player.setPos(new Point(col, row));
                        currentLevel.setTile(row, col, LevelLayout.FLOOR);
                    }
                    case (LevelLayout.PORTAL) -> {
                        portal = new Portal(new Point(col, row));
                        currentLevel.setTile(row, col, LevelLayout.BLOCK);
                    }
                    default -> {
                    }
                }
            }
        }
    }

    //----------------------------------------------
    public Game() {
        levelContainer = new LevelLayoutContainer();
        readFile(MAP_PATH);
        currentLevel = null;
        portal = new Portal();
        player = new Player(new Point(0, 0));
        bombList = new BombList();
        explosionList = new ExplosionList();
        powerUpList = new PowerUpList();
        //enemyList = new EnemyList();
        sprite = new GameSprite();
    }
}
