package int2204.bomberman.BaseGame.StaticObject;

/**
 * Breakable blocks, can be destroyed using bombs.
 */
public class Block extends StaticObject{
    public static boolean DESTRUCTIBLE = true;
    public static boolean COLLISION = true;
}
