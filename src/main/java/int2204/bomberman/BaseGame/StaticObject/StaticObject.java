package int2204.bomberman.BaseGame.StaticObject;

import int2204.bomberman.BaseGame.GameObject;

/**
 * Static object. Handle block, wall, power up, portals that take up 1 full tile.
 */
public class StaticObject extends GameObject {
    public StaticObject() {}

    public static boolean DESTRUCTIBLE = false;
    public static boolean COLLISION = false;
}
