package int2204.bomberman.BaseGame.StaticObject;

/**
 * Open tile, can be walked on.
 * Created when game start and when other static breaks.
 */
public class Floor extends StaticObject {
    public static boolean DESTRUCTIBLE = false;
    public static boolean COLLISION = false;
}
