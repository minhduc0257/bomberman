package int2204.bomberman.BaseGame.StaticObject;

/**
 * Unbreakable wall, cannot be affected by any mean.
 */
public class Wall extends StaticObject {
    public static boolean DESTRUCTIBLE = false;
    public static boolean COLLISION = true;
}
