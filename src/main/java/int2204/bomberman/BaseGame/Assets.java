package int2204.bomberman.BaseGame;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class Assets {
    public static ImageView GetScaledImageView(Image image, double width, double height)
    {
        var view = new ImageView();
        view.setImage(image);
        view.setFitWidth(width);
        view.setFitHeight(height);
        view.setPreserveRatio(true);
        view.setSmooth(true);
        view.setCache(true);
        return view;
    }
}
