package int2204.bomberman.BaseGame.Entity;

import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

/**
 * Controllable character.
 */
public class Player extends Entity {
    private final Point dir;
    private int currentBombSize;
    private int currentBombCount;
    private int currentSpeed;
    private final int BASE_BOMB_COUNT = 1;
    private final int BASE_BOMB_SIZE = 1;
    private final int BASE_TIMER = 200;
    private final int BASE_SPEED = 2;
    private final int MAX_SPEED = 5;
    private boolean alive;

    public Player(Point tilePos) {
        super(tilePos);
        this.dir = new Point(0, 0);
        currentSpeed = BASE_SPEED;
        currentBombCount = BASE_BOMB_COUNT;
        currentBombSize = BASE_BOMB_SIZE;
        alive = true;
    }

    public Player() {
        super(new Point(0, 0));
        this.dir = new Point(0, 0);
        currentSpeed = BASE_SPEED;
        currentBombCount = BASE_BOMB_COUNT;
        currentBombSize = BASE_BOMB_SIZE;
        alive = true;
    }

    //Power up
    public void explosionsUp() {
        System.out.println("Explosion up");
        ++currentBombSize;
    }

    public void bombUp() {
        System.out.println("Bomb up");
        ++currentBombCount;
    }

    public void speedUp() {
        System.out.println("Speed up");
        currentSpeed = Math.min((currentSpeed + 1), MAX_SPEED);
    }

    //General methods
    public void addDir(int valX, int valY) {
        this.dir.setX(this.dir.getX() + valX);
        this.dir.setY(this.dir.getY() + valY);
    }

    public int getCurrentBombCount() {
        return currentBombCount;
    }

    public int getCurrentBombSize() {
        return currentBombSize;
    }

    public int getBaseTimer() {
        return BASE_TIMER;
    }

    public Point getDir() {
        return dir;
    }

    public int getSpeed() {
        return this.currentSpeed;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        if (alive) {
            context.drawImage(sprite.getSprite(GameSprite.PLAYER).getImage(),
                    getPos().getX(), getPos().getY());
        }
    }

    //Handle alive state
    public boolean isAlive() {
        return alive;
    }

    public void reset() {
        dir.setX(0);
        dir.setY(0);
        alive = true;
    }

    /**
     * Kill the player, change alive state to false.
     * Game will still be running until restart.
     */
    public void destroy() {
        alive = false;
    }
}