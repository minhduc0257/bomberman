package int2204.bomberman.BaseGame.Entity;

import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.LevelLayout;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Basic enemy.
 */
public class Enemy extends Entity {
    public static final int TYPE1 = 1;
    public static final int TYPE2 = 2;

    public static final int SPEED_NORMAL = 2;
    public static final int SPEED_FAST = 4;

    private boolean followingPlayer;
    private int speed;

    private final Queue<Point> dirList;
    private Point currentDir;
    private int currentMoveLeft;

    private final int type;

    public Enemy(Point tilePos, int type) {
        super(tilePos);
        this.type = type;
        dirList = new LinkedList<>();
        speed = SPEED_NORMAL;
    }

    public void goToNextTile() {
        currentDir = dirList.peek();
        dirList.remove();
    }

    public void addPath(Point dir) {
        dirList.add(dir);
    }

    public boolean finishedPath() {
        return currentMoveLeft <= 0 && dirList.isEmpty();
    }

    public Point getCurrentDir() {
        return currentDir;
    }

    public int getCurrentMoveLeft() {
        return currentMoveLeft;
    }

    public void setCurrentMoveLeft(int currentMoveLeft) {
        this.currentMoveLeft = currentMoveLeft;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getType() {
        return type;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        switch (type) {
            case 1 -> context.drawImage(sprite.getSprite(GameSprite.ENEMY_1).getImage(),
                    getPos().getX(), getPos().getY());

            case 2 -> context.drawImage(sprite.getSprite(GameSprite.ENEMY_2).getImage(),
                    getPos().getX(), getPos().getY());
        }
    }

    /**
     * Kill the enemy, gain point for the player.
     */
    public void destroy() {

    }
    //TODO: A lot.
}
