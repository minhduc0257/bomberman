package int2204.bomberman.BaseGame.Entity;

import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.InteractiveObject.Portal;
import int2204.bomberman.BaseGame.LevelLayout;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;

public class EnemyList {
    ArrayList<Enemy> list;

    public void addEnemy(Point tilePos, int type) {
        System.out.println("Created enemy type " + type + " at " + tilePos);
        list.add(new Enemy(tilePos, type));
    }

    public ArrayList<Enemy> getList() {
        return list;
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        for (Enemy enemy : list) {
            enemy.drawToCanvas(context, sprite);
        }
    }

    public EnemyList() {
        list = new ArrayList<>();
    }
}
