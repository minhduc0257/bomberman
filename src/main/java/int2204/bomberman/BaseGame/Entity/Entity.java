package int2204.bomberman.BaseGame.Entity;


import int2204.bomberman.BaseGame.GameObject;
import int2204.bomberman.BaseGame.GameSprite;
import int2204.bomberman.BaseGame.Point;
import javafx.scene.canvas.GraphicsContext;

/**
 * Entity class, handle movable entities.
 */
public class Entity extends GameObject {
    private final Point pos;

    public void moveBy(int x, int y) {
        pos.setX(pos.getX() + x);
        pos.setY(pos.getY() + y);
    }

    public Entity(Point tilePos) {
        pos = new Point(
                tilePos.getX() * GameSprite.SIZE,
                tilePos.getY() * GameSprite.SIZE);
    }

    public void setPos(Point tilePos) {
        pos.setX(tilePos.getX() * GameSprite.SIZE);
        pos.setY(tilePos.getY() * GameSprite.SIZE);
    }

    public boolean isPointOnTop(Point point) {
        return getPos().getX() <= point.getX() && point.getX() <= getMaxPos().getX() &&
                getPos().getY() <= point.getY() && point.getY() <= getMaxPos().getY();
    }
    /**
     * Get the position in tile instead of pixel
     */
    public Point getTile() {
        int x = (this.getPos().getX() + GameSprite.SIZE / 2) / GameSprite.SIZE;
        int y = (this.getPos().getY() + GameSprite.SIZE / 2) / GameSprite.SIZE;
        return new Point(x, y);
    }

    /**
     * Get the player pos, at the northwest position.
     */
    public Point getPos() {
        return pos;
    }

    /**
     * Get the pos in the southeast side.
     */
    public Point getMaxPos() {
        return new Point(
                pos.getX() + GameSprite.SIZE - 1,
                pos.getY() + GameSprite.SIZE - 1
        );
    }
}
