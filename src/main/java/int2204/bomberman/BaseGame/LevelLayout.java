package int2204.bomberman.BaseGame;

import int2204.bomberman.BaseGame.InteractiveObject.Portal;
import int2204.bomberman.BaseGame.StaticObject.Block;
import int2204.bomberman.BaseGame.StaticObject.Floor;
import int2204.bomberman.BaseGame.StaticObject.Wall;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.Vector;

public class LevelLayout {
    //Static group
    public static final char WALL = '#';
    public static final char BLOCK = '*';
    public static final char FLOOR = ' ';
    public static final char PORTAL = 'x';
    //Moving group
    public static final char PLAYER = 'p';
    public static final char ENEMY1 = '1';
    public static final char ENEMY2 = '2';

    private final ArrayList<ArrayList<Character>> layout;
    private int row;
    private int col;

    public static boolean isDestructible(char tile) {
        switch (tile) {
            case BLOCK -> {
                return Block.DESTRUCTIBLE;
            }
            case FLOOR -> {
                return Floor.DESTRUCTIBLE;
            }
            case WALL -> {
                return Wall.DESTRUCTIBLE;
            }
        }
        return false;
    }

    public static boolean isCollision(char tile) {
        switch (tile) {
            case BLOCK -> {
                return Block.COLLISION;
            }
            case FLOOR -> {
                return Floor.COLLISION;
            }
            case WALL -> {
                return Wall.COLLISION;
            }
        }
        return false;
    }

    public LevelLayout(ArrayList<ArrayList<Character>> layout, int row, int col) {
        this.layout = layout;
        this.row = row;
        this.col = col;
    }

    public LevelLayout(LevelLayout layout) {
        this.layout = layout.getLayout();
        row = layout.getRow();
        col = layout.getCol();
    }

    public LevelLayout() {
        layout = new ArrayList<>();
    }

    public void drawToCanvas(GraphicsContext context, GameSprite sprite) {
        for (int row = 0; row < this.getRow(); row++) {
            for (int col = 0; col < this.getCol(); col++) {
                ImageView img = switch (this.getTile(row, col)) {
                    case WALL -> sprite.getSprite(GameSprite.WALL);
                    case BLOCK -> sprite.getSprite(GameSprite.BLOCK);
                    default -> sprite.getSprite(GameSprite.FLOOR);
                };
                context.drawImage(img.getImage(),
                        col * GameSprite.SIZE, row * GameSprite.SIZE);
            }
        }
    }

    public void destroyTile(Point p) {
        if (LevelLayout.isDestructible(getTile(p))) {
            char c1 = getTile(p);
            setTile(p.getY(), p.getX(), LevelLayout.FLOOR);
            char c2 = getTile(p);
        }
    }

    public void setTile(int row, int col, char tile) {
        layout.get(row).set(col, tile);
    }

    public Character getTile(int row, int col) {
        return layout.get(row).get(col);
    }

    public char getTile(Point p) {
        int c = p.getX();
        int r = p.getY();
        return getTile(r, c);
    }

    public char getTileFromPixel(Point p) {
        int c = p.getX() / GameSprite.SIZE;
        int r = p.getY() / GameSprite.SIZE;
        return getTile(r, c);
    }

    public ArrayList<ArrayList<Character>> getLayout() {
        return layout;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                returnString.append(getTile(i, j));
            }
            returnString.append("\n");
        }
        return returnString.toString();
    }
}
