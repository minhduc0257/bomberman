module int2204.bomberman {
    requires javafx.controls;
    requires javafx.fxml;


    opens int2204.bomberman to javafx.fxml;
    exports int2204.bomberman;
    exports int2204.bomberman.BaseGame;
    opens int2204.bomberman.BaseGame to javafx.fxml;
    exports int2204.bomberman.BaseGame.InteractiveObject;
    opens int2204.bomberman.BaseGame.InteractiveObject to javafx.fxml;
}